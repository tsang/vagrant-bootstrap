#!/usr/bin/env bash

# Drush
DRUSH_VERSION="8.1.7" # current Drush release from https://github.com/drush-ops/drush/releases

echo "[KCI provisioning] Switching user to 'vagrant'"
echo "[KCI provisioning] Installing composer..."
# install Composer
curl -s https://getcomposer.org/installer | php > /dev/null
sudo mv composer.phar /usr/local/bin/composer > /dev/null

echo "[KCI provisioning] Installing drush..."
# install & update drush
sudo wget -q https://github.com/drush-ops/drush/archive/$DRUSH_VERSION.tar.gz > /dev/null # download drush from github
sudo tar -C /opt/ -xzf $DRUSH_VERSION.tar.gz > /dev/null # untar drush in /opt
sudo chown -R vagrant:vagrant /opt/drush-$DRUSH_VERSION > /dev/null # ensure the vagrant user has sufficient rights
sudo ln -s /opt/drush-$DRUSH_VERSION/drush /usr/sbin/drush > /dev/null # add drush to /usr/sbin
sudo rm -rf /home/vagrant/$DRUSH_VERSION.tar.gz > /dev/null # remove the downloaded tarbal
cd /opt/drush-$DRUSH_VERSION > /dev/null
composer install > /dev/null

echo "[KCI provisioning] Adding paths..."
touch /home/vagrant/.bash_profile
touch /home/vagrant/.bashrc
# add composer path
echo 'export PATH="$HOME/.config/composer/vendor/bin:$PATH' | tee -a /home/vagrant/.bash_profile

BASHRC=$(cat <<EOF
alias reload='source ~/.bash_profile'
alias cdw='cd /var/www/html'
alias d='drush'
alias cc='drush cc all'
alias g='git'
alias gs='git status'
alias co='git checkout'
alias gitup='git pull'
alias gup="git pull"
alias l="ls -lah"
EOF
)
echo "${BASHRC}" >> /home/vagrant/.bashrc
