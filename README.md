### Description ###

Vagrant Provisioning scripts


*This configuration assumes the local environment is macOS*

### Deployment ###

Clone to your desired local directory (~/Sites)

Navigate to the directory in Terminal

From command line, run
```
#!ruby

vagrant up
```

Go grab a coffee