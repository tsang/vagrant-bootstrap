# -*- mode: ruby -*-
# vi: set ft=ruby :

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

  # Every Vagrant virtual environment requires a box to build off of.
  config.vm.box = "ubuntu/trusty64"

  config.vm.define "KCI Development" do |kci|

    # Message to show after 'vagrant up'
    config.vm.post_up_message = "Welcome to the KCI development environment"

    # Create a private network, which allows host-only access to the machine using a specific IP.
    config.vm.network "private_network", ip: "192.168.33.22"

    # Create machine hostname, defaults to nil
    config.vm.hostname = "kci-vagrant"

    # Share an additional folder to the guest VM. The first argument is the path on the host to the actual folder.
    # The second argument is the path on the guest to mount the folder.
    config.vm.synced_folder "~/Sites", "/var/www/html", create: true, group: "www-data", owner: "www-data"

    # Define the base bootstrap file: A (shell) script that runs after first setup of your box
    config.vm.provision :shell do |sh|
      sh.path = "bootstrap.sh"
    end

    # Define the user bootstrap file: A (shell) script that runs 'vagrant' user setup
    config.vm.provision :shell do |sh|
      sh.privileged = false
      sh.path = "user.sh"
    end

  end

end
