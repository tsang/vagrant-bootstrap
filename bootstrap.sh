#!/usr/bin/env bash

#### VARIABLES #####
# Use single quotes instead of double quotes to make it work with special-character passwords
PASSWORD='password'

# Locale
LOCALE_LANGUAGE="en_US"
LOCALE_CODESET="en_US.UTF-8"

# Timezone
TIMEZONE="America/Yellowknife"

echo "[KCI provisioning] Setting locale..."
sudo locale-gen $LOCALE_LANGUAGE $LOCALE_CODESET > /dev/null

echo "[KCI provisioning] Updating..."
# update / upgrade
sudo apt-get update > /dev/null
sudo apt-get -y upgrade > /dev/null

##### PROVISION LAMP STACK #####
echo "[KCI provisioning] Installing LAMP stack..."
# install apache 2.5 and php 5.5
sudo apt-get install -y apache2 > /dev/null
sudo apt-get install -y php5 > /dev/null


echo "[KCI provisioning] Configuring VirtualHost..."
# create httpd.conf
touch /etc/apache2/httpd.conf
# setup hosts file
VHOST=$(cat <<EOF
DocumentRoot "/var/www/html"
<Directory "/var/www/html">
    Options Indexes FollowSymLinks
    AllowOverride All
    Require all granted
</Directory>

<IfModule dir_module>
    DirectoryIndex index.php index.html
</IfModule>

<FilesMatch \.php$>
    SetHandler application/x-httpd-php
</FilesMatch>

ServerName localhost

<VirtualHost *>
    UseCanonicalName Off
    VirtualDocumentRoot /var/www/html/%0
</VirtualHost>

EOF
)
echo "${VHOST}" >> /etc/apache2/httpd.conf

echo "[KCI provisioning] Configuring MySQL..."
# install mysql and give password to installer
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password $PASSWORD" > /dev/null
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $PASSWORD" > /dev/null
sudo apt-get -y install mysql-server > /dev/null
sudo apt-get install php5-mysql > /dev/null

echo "[KCI provisioning] Configuring PHP..."
# install phpmyadmin and give password(s) to installer
# for simplicity I'm using the same password for mysql and phpmyadmin
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/dbconfig-install boolean true" > /dev/null
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/app-password-confirm password $PASSWORD" > /dev/null
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/admin-pass password $PASSWORD" > /dev/null
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/app-pass password $PASSWORD" > /dev/null
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2" > /dev/null
sudo apt-get -y install phpmyadmin > /dev/null

# enable mod_rewrite
sudo a2enmod rewrite

# enable mod_vhost_alias
sudo a2enmod vhost_alias

echo 'upload_max_filesize = 100M' >> /etc/php5/apache2/conf.d/user.ini | tee -a
echo 'post_max_size = 100M' >> /etc/php5/apache2/conf.d/user.ini | tee -a
echo 'max_execution_time = 300' >> /etc/php5/apache2/conf.d/user.ini | tee -a
echo 'max_input_time = 120' >> /etc/php5/apache2/conf.d/user.ini | tee -a

# restart apache
service apache2 restart

echo "[KCI provisioning] Installing git..."
# install git
sudo apt-get -y install git > /dev/null

echo "[KCI provisioning] Configuring vagrant user..."
usermod -a -G vagrant www-data > /dev/null # adds vagrant user to www-data group
